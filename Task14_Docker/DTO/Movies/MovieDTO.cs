﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13_MovieAPI.DTO.Movies
{
    //DTO object for Movie model
    public class MovieDTO
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public DateTime ReleseYear { get; set; }
        public string Director { get; set; }
        public Uri Trailer { get; set; }
        public string Franchise { get; set; }
        public List<string> MovieCharacters { get; set; }
    }
}
