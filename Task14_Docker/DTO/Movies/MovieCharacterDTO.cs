﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13_MovieAPI.DTO.Movies
{
    //DTO object for MovieCharacter model
    public class MovieCharacterDTO
    {
        public int MovieId { get; set; }
        public int CharacterId { get; set; }
        public Uri Picture { get; set; }
        public int ActorId { get; set; }
    }
}
