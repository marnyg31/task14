﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Task13_MovieAPI.DTO.Franchises;
using Task13_MovieAPI.Models;

namespace Task13_MovieAPI.Profiles.Franchises
{
    //Profile for automapper to map between Franchise and its DTO
    public class FranchiseProfile:Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDTO>()
                .ForMember(
                    fDTO => fDTO.MoviesInFranchise,
                    opt => opt.MapFrom(
                        f => f.Movies.Select(m => m.MovieTitle)));
        }
    }
}
