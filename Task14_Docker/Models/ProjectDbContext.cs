using Microsoft.EntityFrameworkCore;

namespace Task13_MovieAPI.Models
{
    //Database context for this project. The database configuration in done in the startup.cs file
    public class ProjectDbContext : DbContext
    {
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options) : base(options) { }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId });
            // modelBuilder.Entity<Actor>().HasOne(a=>a.MovieCharacter).()
        }
    }
}
