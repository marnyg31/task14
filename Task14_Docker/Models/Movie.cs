﻿using System;
using System.Collections.Generic;

namespace Task13_MovieAPI.Models
{
    //Model definition for Movie
    public class Movie
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public DateTime ReleseYear { get; set; }
        public string  Director { get; set; }
        public Uri Trailer { get; set; }
        public Franchise Franchise { get; set; }
        public int FranchiseId { get; set; }
        public List<MovieCharacter> MovieCharacter { get; set; }
    }
}
