﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13_MovieAPI.DTO.Characters;
using Task13_MovieAPI.DTO.Franchises;
using Task13_MovieAPI.DTO.Movies;
using Task13_MovieAPI.Models;

namespace Task13_MovieAPI.Controllers
{

    //This is the controller class for the Franchise model it defines the endpoints for reading and manipulating the table in the database
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly ProjectDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(ProjectDbContext context,IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchises()
        {
            var franchises= await _context.Franchises.Include(f=>f.Movies).ToListAsync();
            return franchises.Select(f => _mapper.Map<FranchiseDTO>(f)).ToList();
        }
        // GET: api/Franchises/Movies/{id}
        [HttpGet("{franchiseId}/[action]")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> Movies(int franchiseId)
        {
            var franchise = await _context.Franchises.
                Include(f => f.Movies)
                .FirstOrDefaultAsync(f => f.Id == franchiseId);

            if (franchise == null || franchise.Movies.Count == 0) return NotFound();
            return franchise.Movies.Select(m => _mapper.Map<MovieDTO>(m)).ToList();
        }

        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseDTO>(franchise);
        }

        // GET: api/Franchises/5/Actors
        [HttpGet("{id}/Actors")]
        public async Task<ActionResult<List<CharacterDTO>>> GetActorsInFranchise(int id)
        {
            var franchise = await _context.Franchises.
                Include(f=>f.Movies).
                ThenInclude(m=>m.MovieCharacter).
                ThenInclude(mc=>mc.Character)
                .Where(f=>f.Id==id).FirstOrDefaultAsync();

            if (franchise == null)
            {
                return NotFound();
            }


            var movieCharactersPerMovieInFranchise = franchise.Movies.Select(m=>m.MovieCharacter);
            var charactersAcrossAllMoviesInFranhcise = movieCharactersPerMovieInFranchise.Select(mcList => mcList.Select(mc => mc.Character));
            var AllCharacters = charactersAcrossAllMoviesInFranhcise.SelectMany(charactersInMovies => charactersInMovies).Distinct();

            return AllCharacters.Select(c=>_mapper.Map<CharacterDTO>(c)).ToList();
        }
        
        // PUT: api/Franchises/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FranchiseDTO>> PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id },_mapper.Map<FranchiseDTO>(franchise));
        }

        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FranchiseDTO>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return _mapper.Map<FranchiseDTO>(franchise);
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
