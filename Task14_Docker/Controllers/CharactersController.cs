﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13_MovieAPI.DTO.Actors;
using Task13_MovieAPI.DTO.Characters;
using Task13_MovieAPI.Models;

namespace Task13_MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        //This is the controller class for the Character model it defines the endpoints for reading and manipulating the table in the database
        private readonly ProjectDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(ProjectDbContext context,IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharacters()
        {
            var characters=await _context.Characters.Include(c=>c.MovieCharacter).ThenInclude(mc=>mc.Movie).ToListAsync();
            return characters.Select(c => _mapper.Map<CharacterDTO>(c)).ToList();
        }

        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> GetCharacter(int id)
        {
            var character=await _context.Characters.Include(c=>c.MovieCharacter).ThenInclude(mc=>mc.Movie).FirstOrDefaultAsync(c=>c.Id==id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterDTO>(character);
        }

        // GET: api/Characters/5/Actors
        [HttpGet("{id}/Actors")]
        public async Task<ActionResult<List<ActorDTO>>> GetActorsForCharacter(int id)
        {
            var character=await _context.Characters.
                Include(c=>c.MovieCharacter).
                ThenInclude(mc=>mc.Actor).
                FirstOrDefaultAsync(c=>c.Id==id);

            if (character == null)
            {
                return NotFound();
            }

            var actors = character.MovieCharacter.Select(mc => mc.Actor).Distinct();

            return actors.Select(a => _mapper.Map<ActorDTO>(a)).ToList();
        }
        // PUT: api/Characters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CharacterDTO>> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id },_mapper.Map<CharacterDTO>(character));
        }

        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CharacterDTO>> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return _mapper.Map<CharacterDTO>(character);
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
