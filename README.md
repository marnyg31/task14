Solution for task 14

This repo contains the solution for task 14. Here the coder from task 13 is packaged into a docker container and a docker-compose file is supplied to orchestrate the new container together with a sql container. 

To start the project, run `docker-compose up` in the root of the repo. Docker compose will then pull the required images from dockerhub and host the api on localhost port 80. To inspect the endpoint of the api, you can open http://localhost/swagger in your browser.

The image created in this repo is also hosted on dockerhub, at https://hub.docker.com/repository/docker/marnyg31/movie_api. If you want to use your own database, you can override the environment variable section in the docker compose file specifying the connection string. The api will run the migrations at the program start, so the only dependency of the container is that it is able to successfully connect to a database. 

Also, if you want to inspect the database created by docker-compose, the sql container exposes port 1433. So you can connect to the database with this info:

- Server: "localhost, 1433"
- Username: "sa"
- Password: "BigPassw0rd"

The project is also hosted on azure, and can be explored at this url:

http://movieapi.northeurope.azurecontainer.io/
